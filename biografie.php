<?php
  $title = "Biografie";
  $bodyClasses = "sub-page";
  include('includes/menu.php');
  $menuItems['about']['active'] = true;
  include('includes/header.php');
?>
        <h2 class="hidden"><?php echo $title; ?></h2>
        <article class="with-image">
          <h2>Sabrien Mari</h2>
          <img src="assets/img/elements/sabrien-square.jpg" height="200" width="200" alt="">
          <p>"Wenn das Leben dir Steine in den Weg legt, dann nutze sie, um dein Fundament zu stärken!"
Seit 10 Jahren schon erobert Sabrien Mari die Bühnen mit ihrer unendlichen Power. Ob es die sanften Töne und die geballte Stimmkraft , ihre offenen und freche Art oder die Nähe zum Publikum ist, mit der sie alle in den Bann zieht, das muss man selbst entscheiden.
Um Ihren Abend unvergesslich zu machen ist Sabrien mit größter Leidenschaft und vollem Herzen bei der Sache.
Mit ihrer Bühnenpräsenz, ihrer Ausstrahlung, und ihrem Band-Partner Uwe, machen sie zusammen als Heart&Soul jede Veranstaltung zu etwas ganz besonderem! </p>
        </article>
        <article class="with-image">
          <h2>Uwe Gravemeier</h2>
          <img src="assets/img/elements/uwe-square.jpg" height="200" width="200" alt="">
          <p>Uwe Gravemeier, klassisch ausgebildeter Pianist, ist Herr der Tasten von Stagepiano und Keyboard und gibt den Songs den passenden Sound.
            Seit 2008 ist er in verschiedenen Bands unterwegs. Nun hat er schon seit über einem Jahr das exakt passende Gegenstück zu seiner Art, Musik zu machen, zu leben und zu genießen, gefunden.</p>
          <p>Mit seiner Akustik Gitarre und dem Background Gesang wird das musikalische Ensemble mit Sabrien Mari als Gesamtpaket perfekt und begeistert auf voller Linie.</p>
        </article>
<?php include('includes/footer.php'); ?>