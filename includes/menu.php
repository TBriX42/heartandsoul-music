<?php

$menuItems = array();

$menuItems['about']['href'] = "about.php";
$menuItems['about']['text'] = "Über uns";
$menuItems['about']['active'] = false;

$menuItems['kontakt']['href'] = "kontakt.php";
$menuItems['kontakt']['text'] = "Kontakt";
$menuItems['kontakt']['active'] = false;

$menuItems['impressum']['href'] = "imprint.php";
$menuItems['impressum']['text'] = "Impressum";
$menuItems['impressum']['active'] = false;

?>