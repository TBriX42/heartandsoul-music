var state = 0;
var changedByProg = false;

jQuery(function() {
  jQuery('html').removeClass('no-js').addClass('js');

  if (jQuery('body').hasClass('front')) {
    var fullHeight = jQuery("#header-lower-wrapper").height();
    jQuery("#header-lower-wrapper").delay(2000).fadeIn(600, function() {
      jQuery(this).trigger('visible');
    });
  } else {
    jQuery("#header-lower-wrapper").trigger('visible');
  }

  jQuery('#main-menu a, .internal-link').click(function(e) {
    e.preventDefault();
    loadPage(jQuery(this).attr('href'));
  });

  // menu toggle
  jQuery('.menu-toggle').click(function() {
    var $menu = jQuery(jQuery(this).attr('data-toggle'));
    toggleMenu($menu);

    $menu.click(function() {
      toggleMenu(jQuery(this));
    });
  });
});

function toggleMenu($menu) {
  var maxValue = $menu.css('max-height');
  var fullHeight = $menu.css('max-height', 'none').height();
  $menu.css('max-height', maxValue);

  if (!$menu.hasClass('open')) {
    $menu.css({'overflow':'hidden', 'max-height': 0}).show().animate({
      'max-height': fullHeight
    }, 500, function() {
      jQuery(this).removeClass('closed').addClass('open');
    });
  } else {
    $menu.animate({
      'max-height': 0
    }, 500, function() {
      jQuery(this).removeClass('open').addClass('closed');
    });
  }
}

function loadPage(url) {
  jQuery('body').append('<div id="ajax" class="hidden"></div>');
  jQuery('#ajax').load(url + ' #main', function() {
          console.log(url);
    jQuery('#site-wrapper #main').fadeToggle(500, function() {
      var bodyClasses = jQuery('#ajax #main').attr('data-body-classes');
      //jQuery('body').attr('class', bodyClasses);
      // animate header
      if (jQuery("body").hasClass('front')) {
        jQuery("body").removeClass('front').addClass('sub-page');
       // console.log(jQuery('#header').css('height', 'auto').height());
        var autoHeight = jQuery('#header').height()-148;
        jQuery('#header').css('height', '100%').animate({
          height: autoHeight
        }, 1000, 'swing', function() {
          jQuery(this).css('height', 'auto');
          changeContent(url);
        });
      } else {
        changeContent(url);
      }

      jQuery('.internal-link').click(function(e) {
        e.preventDefault();
        loadPage(jQuery(this).attr('href'));
      });
    });
  });
}

function changeContent(url) {
  changedByProg = true;
  History.pushState({state:state}, jQuery("#ajax #main").attr("data-title"), url.replace(window.location.protocol + "://" + window.location.hostname + "/"));
  changedByProg = false;
  state++;

  jQuery('#site-wrapper #main').html(jQuery('#ajax #main').html()).fadeToggle(500);
  jQuery('#main-menu ul li').removeClass('active');
  jQuery("#main-menu ul li a[href*='"+url+"']").parent().addClass("active");
  jQuery('#ajax').remove();
}