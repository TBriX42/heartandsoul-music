jQuery(function() {
  var audio = jQuery('#header-lower-wrapper audio').get()[0];

  jQuery("#header-lower-wrapper").on('visible', function() {
    audio.play();
  });

  if (!jQuery('body').hasClass('front'))
    audio.play();

  jQuery('.pause, .play').click(function() {
    if (audio.paused) {
      audio.play();
      jQuery(this).removeClass('play').addClass('pause');
    }
    else {
      audio.pause();
      jQuery(this).removeClass('pause').addClass('play');
    }
  });

  jQuery("#seek-slider").slider({
    step: 0.1,
    slide: function( event, ui ) {
      audio.currentTime = audio.seekable.end(0) * (ui.value/100);
    }
  });

  audio.ontimeupdate = function() {
    jQuery("#seek-slider").slider('value', (audio.currentTime / audio.seekable.end(0))*100);
  };
});