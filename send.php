<?php
require 'PHPMailer/PHPMailerAutoload.php';

$birthday = getData('birthday');
if (!empty($birthday))
  goBack();

$error = array();

$email = getData('email');
$name = getData('name');
$subject = getData('subject');
$message = getData('message');

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  $error[] = "Die eingegebene E-Mail Adresse ist nicht gültig!";
}

if (trim($name) == "")
  $error[] =  "Bitte geben Sie einen Namen ein!";

if (trim($subject) == "")
  $error[] =  "Bitte geben Sie einen Betreff ein!";


if (trim($message) == "")
  $error[] =  "Bitte geben Sie eine Nachricht ein!";

if (count($error) == 0) {
  $message = "E-Mail: ".$email."\n\n".$message;
  $mail = new PHPMailer;

  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.strato.de';  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = 'contact@heartandsoul-music.com';                 // SMTP username
  $mail->Password = '';                           // SMTP password
  $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 465;// TCP port to connect to
  $mail->CharSet = 'utf-8';
  $mail->SetLanguage ("de");

  $mail->From = $email;
  $mail->FromName = $name;
  $mail->addAddress('contact@heartandsoul-music.com', 'Webseite Kontaktformular');     // Add a recipient

  $mail->isHTML(true);                                  // Set email format to HTML

  $mail->Subject = $subject;
  $mail->Body    = nl2br($message);
  $mail->AltBody = $message;

  if(!$mail->send()) {
      $error[] = 'E-Mail konnte nicht gesendet werden.';
  } else {
      goBack(array('success' => 'Nachricht wurde erfolgreich versendet.'));
  }
}

goBack(array('email' => $email,
             'name' => $name,
             'subject' => $subject,
             'message' => $message,
             'error' => implode('|', $error)));



function goBack($data = null) {
  $dataStr = "?";

  if ($data != null) {
    foreach ($data as $key => $value) {
      $dataStr .= $key."=".$value."&";
    }
  }

  header("Location: kontakt.php".$dataStr);
}

function getData($data) {
  if (isset($_POST[$data])) {
    return $_POST[$data];
  }

  return "";
}