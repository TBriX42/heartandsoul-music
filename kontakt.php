<?php
  $title = "Kontakt";
  $bodyClasses = "sub-page";
  include('includes/menu.php');
  $menuItems['kontakt']['active'] = true;
  include('includes/header.php');

  $email = getData('email');
  $name = getData('name');
  $subject = getData('subject');
  $message = getData('message');

  function getData($data) {
    if (isset($_GET[$data])) {
      return $_GET[$data];
    }

    return "";
  }
?>
        <h2><?php echo $title; ?></h2>
        <div class="row responsive">
          <div class="col-6">
            <?php if (isset($_GET['error'])) : ?>
              <div class="error-message">
                <?php
                  $errors = explode('|', getData('error'));
                  foreach ($errors as $error) {
                    echo "<p>".$error."</p>";
                  }
                ?>
              </div>
            <?php elseif (isset($_GET['success'])) :?>
              <div class="success-message"><?php echo getData('success'); ?></div>
            <?php endif; ?>
            <form action="send.php" method="post" accept-charset="utf-8">
              <input type="email" value="<?php echo $email; ?>" name="email" placeholder="E-Mail">
              <input type="text" value="<?php echo $name; ?>" name="name" placeholder="Name">
              <input type="text" value="<?php echo $subject; ?>" name="subject" placeholder="Betreff">
              <input type="text" name="birthday" placeholder="Birthday" class="birthday">
              <textarea name="message" placeholder="Nachricht"><?php echo $message; ?></textarea>
              <input type="submit" name="" value="Senden">
            </form>
          </div>
          <aside class="col-6">
            <dl class="dl-horizontal lowercase">
              <dt>Tel.</dt>
              <dd>+49 5485 938217</dd>
              <dt>Fax</dt>
              <dd>+49 5485 938210</dd>
              <dt>Mail</dt>
              <dd><a href="mailto:contact@heartandsoul-music.com?subject=Anfrage">contact@heartandsoul-music.com</a></dd>
              <dt>Internet</dt>
              <dd><a href="http://www.heartandsoul-music.com">www.heartandsoul-music.com</a></dd>
            </dl>
          </aside>
        </div>
<?php include('includes/footer.php'); ?>