<?php
  $title = "Impressum";
  $bodyClasses = "sub-page";
  include('includes/menu.php');
  $menuItems['impressum']['active'] = true;
  include('includes/header.php');
?>
        <h2><?php echo $title; ?></h2>
        <div class="row responsive">
          <div class="col-6">
            <strong>Angaben gemäß § 5 Telemediengesetz (TMG):</strong><br/>
            Uwe Gravemeier <br/>
            Kattenvenner Str. 84<br/>
            49549 Ladbergen<br/>
            Telefon: +49 5485 938217<br/>
            E-Mail: <a href="mailto:contact@heartandsoul-music.com?subject=Anfrage">contact@heartandsoul-music.com</a><br/><br/>

            <strong>Inhaltlich Verantwortlicher gem. § 55 II RStV:</strong><br/>
            Uwe Gravemeier (Anschrift s. o.)<br/><br/>

            <strong>Gestaltung ung Umsetzung:</strong><br/>
            Thorben Ziegler<br/>
            Riedeweg 218<br/>
            27755 Delmenhorst<br/>
            Telefon: +49 4221 1234428<br/>
            E-Mail: <a href="mailto:contact@thorbenziegler.de">contact@thorbenziegler.de</a><br/>
            <a href="http://www.thorbenziegler.de" title="Thorben Ziegler">www.thorbenziegler.de</a>
          </div>
        </div>
<?php include('includes/footer.php'); ?>