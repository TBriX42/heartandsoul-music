<?php
$selfUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$hostUrl = 'http://'.$_SERVER['HTTP_HOST'];
?>

<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html lang="de" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="de" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="de" class="no-js ie8 oldie"> <![endif]-->
<!--[if IE 9]>    <html lang="de" class="no-js ie9"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 9]><!--> <html lang="de" class="no-js" itemscope="" itemtype="http://schema.org/Product"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!--<meta name="robots" content="noindex">--> <!-- only for dev on subdomain remove on production mode -->

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Bio | heart & soul</title>

  <link rel="shortcut icon" href="/favicon.png" type="image/png">

  <meta name="author" content="Thorben Ziegler">

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300italic,600,600italic' rel='stylesheet' type='text/css'>

  <meta name="description" itemprop="description" content="">

  <meta name="keywords" content="">

  <link rel="canonical" href="<?php echo $selfUrl; ?>" />
  <meta property="og:title" content="" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="<?php echo $selfUrl; ?>" />
  <meta property="og:image" content="<?php echo $hostUrl; ?>/assets/img/elements/og_image.jpg" />
  <meta property="og:site_name" content="" />
  <meta property="og:description" content="" />
  <meta property="article:publisher" content="https://www.facebook.com/" />
  <meta property="article:published_time" content="2015-01-01T12:02:48Z" />
  <meta property="article:modified_time" content="2015-01-01T12:02:48Z" />

  <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="assets/css/normalize.css">
  <link rel="stylesheet" href="assets/css/main.css">
</head>

<body class="sub-page">
  <div id="site-wrapper">
    <div class="container clearfix">
      <header id="header">
        <h1 class="hidden">heart &amp; soul</h1>
        <div id="header-content-wrapper">
          <div id="header-upper-wrapper" class="row">
            <div class="col-6 slogan"><img src="assets/img/elements/slogan.svg" width="100%" alt="Musik von Herzen"></div>
            <a id="logo" class="col-6" href=""><img src="assets/img/elements/logo.svg" width="100%" alt="heart & soul" /></a>
          </div>
          <div id="header-lower-wrapper" class="row">
            <audio preload="auto">
              <source src="assets/sounds/Jenifer_Brening_(cover-_Sabrien_Mari)_-_Not_that_Guy.mp3"/>
              <source src="assets/sounds/Jenifer_Brening_(cover-_Sabrien_Mari)_-_Not_that_Guy.ogg"/>
              Your browser doesn't support html5 audio.
            </audio>
            <div class="controls">
              <div id="seek-slider"></div>
              <a class="pause"></a>
            </div>
          </div>
        </div>
      </header><!-- /header -->
      <section id="main" role="main" data-body-classes="sub-page" data-title="Bio | heart & soul">
        <h2>Biografie</h2>
        <article class="with-image">
          <h2>Uwe Gravemeier</h2>
          <img src="assets/img/elements/32-square.jpg" height="200" width="200" alt="">
          <p>Uwe Gravemeier, klassisch ausgebildeter Pianist, ist Herr der Tasten von Stagepiano und Keyboard und gibt den Songs den passenden Sound.
            Seit 2008 ist er in verschiedenen Bands unterwegs. Nun hat er schon seit über einem Jahr das exakt passende Gegenstück zu seiner Art, Musik zu machen, zu leben und zu genießen, gefunden.</p>
          <p>Mit seiner Akustik Gitarre und dem Background Gesang wird das musikalische Ensemble mit Sabrien Mari als Gesamtpaket perfekt und begeistert auf voller Linie.</p>
        </article>
      </section><!-- /section -->
      <footer id="footer">
        <nav class="main-menu">
          <h1 class="hidden">Hauptmenü</h1>
          <ul>
            <li><a href="bio.php">Biografie</a></li>
            <li><a href="kontakt.php">Kontakt</a></li>
            <li><a href="">Impressum</a></li>
          </ul>
        </nav>
      </footer><!-- /footer -->
    </div>
    <a target="_blank" id="thorben-ziegler" href="http://www.thorbenziegler.de">www.thorbenziegler.de</a>
  </div>

  <!-- Grab Google CDN's jQuery, fall back to local if offline -->
  <!-- 2.0 for modern browsers, 1.10 for .oldie -->
  <script>
  var oldieCheck = Boolean(document.getElementsByTagName('html')[0].className.match(/\soldie\s/g));
  if(!oldieCheck) {
  document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"><\/script>');
  } else {
  document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
  }
  </script>
  <script>
  if(!window.jQuery) {
  if(!oldieCheck) {
    document.write('<script src="assets/js/jquery-2.0.2.min.js"><\/script>');
  } else {
    document.write('<script src="assets/js/libs/jquery-1.10.1.min.js"><\/script>');
  }
  }
  </script>

  <script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery.history.js"></script>
  <script type="text/javascript" src="assets/js/main.js"></script>
  <script type="text/javascript" src="assets/js/sound.js"></script>

  </body>
</html>