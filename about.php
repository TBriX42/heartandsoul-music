<?php
  $title = "Über uns";
  $bodyClasses = "sub-page";
  include('includes/menu.php');
  $menuItems['about']['active'] = true;
  include('includes/header.php');
?>
        <h2 class="hidden"><?php echo $title; ?></h2>
        <figure>
          <img src="assets/img/elements/232.jpg" height="531" width="680" alt="">
          <figcaption>Hier sind wir, <strong>heart &amp; soul</strong>, die verr&uuml;ckteste und abgefahrenste Live Band, die ihr euch &uuml;berhaupt vorstellen k&ouml;nnt!</figcaption>
        </figure>
        <div class="row responsive">
          <div class="col-6">
            <p>Immer gute Laune, flippig, elektrisierend, mitrei&szlig;end, so&nbsp;begegnen wir euch auf den musikalischen Ebenen der Rock und Popmusik!</p>
            <p>Wir bieten euch Leidenschaft pur und lieben es besonders, euch anzustecken,&nbsp;so dass die Stimmung immer weiter und weiter steigt. Eine gef&uuml;llte Tanzfl&auml;che zeigt uns, dass ihr mit uns Spaß habt.</p>
            <p><strong>Seid ihr gl&uuml;cklich und ausgelassen, haben wir unser Ziel erreicht.</strong></p>
            <p>Als stete Unterstützung ist immer unsere Moderatorin dabei, die ihre Energie und gute Laune unter euch verbreitet.</p>
            <p>Zur Zeit sind wir haupts&auml;chlich im M&uuml;nsterland und im Raum Hannover unterwegs, m&ouml;chten dabei aber st&auml;ndig unseren Wirkungskreis erweitern.</p>
            <p>Wir variieren zwischen verschiedenen Stilrichtungen, je nach Publikum und Stimmung. Haupts&auml;chlich h&ouml;rt ihr von uns <strong>Rock-</strong> und <strong>Popsongs</strong>, von <strong>Oldies</strong> bis zu den <strong>aktuellen Charts</strong> &ndash;und Schlagern.</p>
          </div>
          <div class="col-6">
            <p>Wir sind f&uuml;r Partys aller Art zu haben, wie z.B. Hochzeiten, Betriebsfeste, Jubil&auml;en, etc.</p>
            <p>Wenn euch unser Sound gef&auml;llt,&nbsp;ihr Songlisten oder Demo CD`s haben m&ouml;chtet, dann <a href="mailto:contact@heartandsoul-music.com?subject=Anfrage%20Songlisten%2C%20Demos">schreibt uns einfach an</a>.</p>
            <p>Wir sorgen f&uuml;r den Rest und machen aus eurem Event ein unvergessliches Erlebnis!<br />
            Stimmung pur, top&nbsp;Performance&nbsp;und falls gew&uuml;nscht&nbsp;Komplett-Moderation!</p>
            <p>Ob es die sanften T&ouml;ne, die geballte Stimmkraft, ihre offene und freche Art oder die N&auml;he zum Publikum ist, mit der sie alle in den Bann zieht, das muss jeder selbst entscheiden.</p>
            <p class="with-logo">Eure Band <img src="assets/img/elements/logo.svg" class="logo-small" alt="heart & soul"></p>
          </div>
        </div>
        <a href="biografie.php" class="internal-link" style="width: 100%; margin-top: 20px;">Ihr wollt mehr &uuml;ber uns wissen? Klickt hier!</a>
<?php include('includes/footer.php'); ?>